package com.wande.airshare;

import android.support.v4.app.DialogFragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

public class AlertDialogManager extends DialogFragment {
	RadioGroup rd;
	int selectId;
	View vi;

	public static AlertDialogManager newInstance(int title) {
		AlertDialogManager frag = new AlertDialogManager();
		Bundle args = new Bundle();
		args.putInt("title", title);
		frag.setArguments(args);
		return frag;
	}

	public void showAlertDialog(Activity context, String title, String message) {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

					}
				});
		alertDialog.create();

		// Showing Alert Message
		alertDialog.show();
	}

}
