package com.wande.airshare;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class ContactAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    
    public ContactAdapter(Activity a, ArrayList<HashMap<String, String>> contacts) {
        activity = a;
        data=contacts;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.contact, null);

        TextView name=(TextView)vi.findViewById(R.id.lblContact);
        TextView num=(TextView)vi.findViewById(R.id.num);
        Button remove = (Button)vi.findViewById(R.id.btnRemove);
        
        remove.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				data.remove(position);
				notifyDataSetChanged();
			}
		});
        
        name.setText(data.get(position).get("name"));
        num.setText(data.get(position).get("num"));
        return vi;
    }
}