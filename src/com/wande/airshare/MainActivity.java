package com.wande.airshare;

import java.util.ArrayList;
import java.util.HashMap;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class MainActivity extends Activity {

	static final int PICK_CONTACT = 1;

	Button importz;
	Button send;
	Button balance;

	ListView contacts;

	EditText amount;
	EditText password;
	EditText number;

	Spinner networks;
	
	String encodedHash = Uri.encode("#");

	ArrayList<String> contactNumber = new ArrayList<String>();
	ArrayList<HashMap<String, String>> contactName = new ArrayList<HashMap<String, String>>();

	AlertDialogManager alret = new AlertDialogManager();

	String cNumber;

	ContactAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		adapter = new ContactAdapter(MainActivity.this, contactName);

		contacts = (ListView) findViewById(R.id.contacts);
		contacts.setEmptyView(findViewById(R.id.empty));

		amount = (EditText) findViewById(R.id.txtAmount);
		password = (EditText) findViewById(R.id.txtPassword);
		number = (EditText) findViewById(R.id.number);

		importz = (Button) findViewById(R.id.btnImpor);
		send = (Button) findViewById(R.id.btnSend);
		balance = (Button) findViewById(R.id.balance);

		networks = (Spinner) findViewById(R.id.networks);
		networks.setEnabled(false);
		balance.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(
						new Intent("android.intent.action.CALL", Uri
								.parse("tel:" + "*556" + encodedHash)), 1);
			}
		});

		importz.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
				intent.setType(Phone.CONTENT_TYPE);
				startActivityForResult(intent, PICK_CONTACT);
			}
		});

		send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String ussd = "";
				if ((contactNumber.size() == 0 && amount.getText().toString() == "")
						|| number.getText().toString() == ""
						|| password.getText().toString() == "") {
					alret.showAlertDialog(MainActivity.this, "Empty",
							"One of the fields is empty");
				} else {
					if (contactNumber.size() == 0) {
						ussd = "*600*" + number.getText().toString() + "*"
								+ amount.getText().toString() + "*"
								+ password.getText().toString() + "*"
								+ encodedHash;
					} else {
						for (int i = 0; i < contactNumber.size(); i++) {

							ussd = "*600*" + contactNumber.get(i) + "*"
									+ amount.getText().toString() + "*"
									+ password.getText().toString() + "*"
									+ encodedHash;
						}
					}

					startActivityForResult(
							new Intent("android.intent.action.CALL", Uri
									.parse("tel:" + ussd)), 1);

				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	String[] numbers = null;
	int i = 0;

	String phoneNumber = null;
	String id = null;
	String name = null;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		numbers = null;
		phoneNumber = null;
		String id = null;
		int i = 0;

		if (resultCode == Activity.RESULT_OK && requestCode == PICK_CONTACT) {

			ContentResolver cr = getContentResolver();
			Cursor cur = cr.query(data.getData(), null, null, null, null);

			if (cur.getCount() > 0) {

				while (cur.moveToNext()) {
					id = cur.getString(cur
							.getColumnIndex(ContactsContract.Contacts._ID));

					name = cur
							.getString(cur
									.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

					if (Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
						Cursor pCur = cr
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);
						numbers = new String[pCur.getCount()];
						while (pCur.moveToNext()) {
							phoneNumber = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							numbers[i] = phoneNumber;
							i++;
						}
						pCur.close();
					}
				}

			}else{
				AlertDialogManager alert = new AlertDialogManager();
				alert.showAlertDialog(this, "Contact error", "the contact you selected do not have any phone number associated with it");
			}
			cur.close();
			if (numbers != null){
			AlertDialog.Builder ad = new AlertDialog.Builder(this);
			ad.setTitle(name);
			
			ad.setItems(numbers, new OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					HashMap<String, String> map = new HashMap<String, String>();

					cNumber = numbers[which];
					contactNumber.add(cNumber);
					number.setText(cNumber);
					map.put("name", name);
					map.put("num", cNumber);
					contactName.add(map);
					contacts.setAdapter(adapter);

				}
			});
			AlertDialog adb = ad.create();
			adb.show();
		}
		}
	}

}
